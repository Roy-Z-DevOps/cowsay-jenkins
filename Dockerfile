FROM node:16-alpine3.11
RUN apk --no-cache add curl
WORKDIR /app
COPY ./src /app
RUN npm install
COPY entry-point.sh /
RUN chmod 744 /entry-point.sh
ENTRYPOINT [ "/entry-point.sh" ]
CMD [ "" ]









